/*
Problem Description
- You are given an integer array A of length N.
- You are also given a 2D integer array B with dimensions M x 2, where

each row

denotes a [L, R] query.
- For each query, you have to find the sum of all elements from L to R

indices

in A (0 - indexed).
- More formally, find A[L] + A[L + 1] + A[L + 2] +... + A[R - 1] + A[R] for each
query.

Problem Constraints
1 <= N, M <= 103
1 <= A[i] <= 105
0 <= L <= R < N

Example Input
Input 1:
A = [1, 2, 3, 4, 5]
B = [[0, 3], [1, 2]]
Input 2:
A = [2, 2, 2]
B = [[0, 0], [1, 2]]

Example Output
Output 1:
[10, 5]

Output 2:
[2, 4]

Example Explanation
Explanation 1:
The sum of all elements of A[0 ... 3] = 1 + 2 + 3 + 4 = 10.
The sum of all elements of A[1 ... 2] = 2 + 3 = 5.
Explanation 2:
The sum of all elements of A[0 ... 0] = 2 = 2.
The sum of all elements of A[1 ... 2] = 2 + 2 = 4.
 */


public class range_sum {

        static int[] find_range_sum(int arr[] , int q[][]){

                int prefix[] = new int[arr.length];
                int Output[] = new int[q.length];

                prefix[0] = arr[0];
                for(int i = 1 ; i < arr.length ; i++)
                        prefix[i] = arr[i] + prefix[i-1];

                for(int i = 0 ; i < q.length ; i++)
                        Output[i] = prefix[ q[i][1] ] - prefix[ q[i][0] ] + arr[ q[i][0] ];

                return Output;
        }

        public static void main(String[] args) {
                int arr[] = {1, 2, 3, 4, 5};
                int q[][] = { {0, 3} ,{1, 2}};

                int Output[] = find_range_sum(arr, q);

                for(int x : Output)
                        System.out.println(x);

        }
        
}

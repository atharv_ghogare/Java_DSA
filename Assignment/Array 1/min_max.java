/*
Problem Description
- Given an array A of size N.
- You need to find the sum of the Maximum and Minimum

elements in the given array.

Problem Constraints
1 <= N <= 105
-109 <= A[i] <= 109

Example Input
Input 1:
A = [-2, 1, -4, 5, 3]
Input 2:
A = [1, 3, 4, 1]

Example Output
Output 1:
1
Output 2:
5
Example Explanation
Explanation 1:
Maximum Element is 5 and Minimum element is -4. (5 + (-4)) = 1.
Explanation 2:
Maximum Element is 4 and Minimum element is 1. (4 + 1) = 5.
 */

public class min_max {

        static int find_min_max(int arr[]){

                int min = Integer.MAX_VALUE;
                int max = Integer.MIN_VALUE;

                for(int i = 0 ; i < arr.length ; i++){

                        if(arr[i] > max)
                                max = arr[i];

                        if(arr[i] < min)
                                min = arr[i];
                }

                return min + max;
        }

        public static void main(String[] args) {
                int arr[] = {1 , 2 , 3 , 4 , 5};

                System.out.println(find_min_max(arr));
        }        
}

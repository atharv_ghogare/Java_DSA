/*
Problem Description
- Given an integer array A of size N.
- In one second, you can increase the value of one element by 1.
- Find the minimum time in seconds to make all elements of the array

equal.

Problem Constraints
1 <= N <= 1000000
1 <= A[i] <= 1000

Example Input
A = [2, 4, 1, 3, 2]
Example Output
8

Example Explanation
We can change the array A = [4, 4, 4, 4, 4]. The time required will be 8

seconds.
 */

public class time_to_equality {

        static int find_time_to_equality(int arr[]){
                int count = 0;

                int max = Integer.MIN_VALUE;
                for(int i = 0 ; i < arr.length ; i++)
                        if(max < arr[i])
                                max = arr[i];

                for(int i = 0 ; i < arr.length ; i++)
                        count = count + (max - arr[i]);

                return count;
        }

        public static void main(String[] args) {
                int arr[] = {2, 4, 1, 3, 2};
                
                System.out.println(find_time_to_equality(arr));
        }
        
}

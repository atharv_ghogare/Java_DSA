/*
Problem Description
- Given an integer array A containing N distinct integers, you have to find all
the leaders in array A. An element is a leader if it is strictly greater than all

the elements to its right side.

NOTE: The rightmost element is always a leader.
Problem Constraints
1 <= N <= 105
1 <= A[i] <= 108

Example Input
Input 1:
A = [16, 17, 4, 3, 5, 2]
Input 2:
A = [5, 4]

Example Output
Output 1:
[17, 2, 5]
Output 2:
[5, 4]

Example Explanation
Explanation 1:
Element 17 is strictly greater than all the elements on the right side to it.
Element 2 is strictly greater than all the elements on the right side to it.
Element 5 is strictly greater than all the elements on the right side to it.

So we will return these three elements i.e [17, 2, 5], we can also return [2,

5, 17] or [5, 2, 17] or any other ordering.

Explanation 2:
Element 5 is strictly greater than all the elements on the right side to it.
Element 4 is strictly greater than all the elements on the right side to it.
So we will return these two elements i.e [5, 4], we can also do any other

ordering.
 */


public class leaders_in_array {
        static void find_leader_elements(int arr[]){

                if(arr.length == 0)
                        return;

                int max = arr[arr.length - 1];

                System.out.print(arr[arr.length - 1] + "   ");

                for(int i = arr.length - 2 ; i >= 0 ; i--){

                        if(arr[i] > max){
                                System.out.print(arr[i] + "   ");
                                max = arr[i];
                        }
                }
        }

        public static void main(String[] args) {
                int arr[] = {1, 2, 3, 4, 5, 2};

                find_leader_elements(arr);
        }
}

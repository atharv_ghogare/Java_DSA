/*
3. WAP to print the sum of n natural numbers.
 */

public class Program3 {
        static int getSum1(int num){
                int sum = 0;
                for(int i = num ; i >= 1 ; i--)
                        sum += i;

                return sum;
        }

        static int getSum2(int num){

                if(num == 0)
                        return 0;
                
                return num + getSum2(num - 1);

        }

        public static void main(String[] args) {
                
                System.out.println(getSum1(10));

                System.out.println(getSum2(10));
        }
}

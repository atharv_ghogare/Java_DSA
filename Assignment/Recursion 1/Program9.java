/*
9. WAP to print string in reverse order.
 */

public class Program9 {
        static void printReverse(String str){

                if(str.length() == 1){
                        System.out.print(str);
                        return;
                }

                printReverse(str.substring(1));

                System.out.print(str.charAt(0));

        }

        static void printReverse2(String str){
                for(int i = str.length() - 1 ; i >= 0 ; i--){
                        System.out.print(str.charAt(i));
                }

                System.out.println();
        }

        public static void main(String[] args) {
                printReverse("abcdefg");

                System.out.println();
                
                printReverse2("abcdefg");

        }
}

/*
2. WAP to display the first 10 natural numbers in reverse order.
 */

public class Program2 {
        static void printNum1(int num){
                for(int i = num ; i >= 1 ; i--)
                        System.out.println(i);
        }

        static void printNum2(int num){

                if(num == 0)
                        return;
                
                System.out.println(num);
                printNum2(num - 1);

        }

        public static void main(String[] args) {
                
                printNum1(10);

                printNum2(10);
        }
}

/*
7. WAP to find the factorial of a number.
 */

public class Program7 {
        static int getFactorial1(int num){
                int fact = 1;
                
                for(int i = 2 ; i <= num ; i++)
                        fact = fact * i;

                return fact;
        }

        static int getFactorial2(int num){

                if(num == 0)
                        return 1;
                
                return num * getFactorial2(num - 1);

        }

        public static void main(String[] args) {
                
                System.out.println(getFactorial1(4));

                System.out.println(getFactorial2(4));
        }      
}

/*
6. WAP to calculate the sum of digits of a given positive integer.
 */

public class Program6 {
        static int getSum1(int num){
                int sum = 0;
                
                while (num != 0) {
                        sum = sum + (num % 10);
                        num = num / 10;
                }

                return sum;
        }

        static int getSum2(int num){

                if(num == 0)
                        return 0;
                
                return (num % 10) + getSum2( num / 10 );

        }

        public static void main(String[] args) {
                
                System.out.println(getSum1(150));

                System.out.println(getSum2(710));
        }          
}

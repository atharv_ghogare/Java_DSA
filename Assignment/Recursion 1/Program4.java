public class Program4 {
        static int getDigitCount1(int num){
                int count = 0;
                
                while (num != 0) {
                        count++;
                        num = num / 10;
                }

                return count;
        }

        static int getDigitCount2(int num){

                if(num == 0)
                        return 0;
                
                return 1 + getDigitCount2( num / 10 );

        }

        public static void main(String[] args) {
                
                System.out.println(getDigitCount1(110));

                System.out.println(getDigitCount2(110));
        }        
}

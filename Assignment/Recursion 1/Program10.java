/*
 * 10. WAP to check whether the given number is palindrome or not.
 */

public class Program10 {
        static boolean chechPalindrome1(int num){

                int temp = num;

                int rev = 0;

                while (temp != 0) {
                        rev = (rev * 10) + (temp % 10);

                        temp = temp / 10;
                }

                if(rev == num)
                        return true;

                return false;

        }


        static int reverseNumber(int num){
                if(num == 0)
                        return 0;

                return  reverseNumber(num / 10) * 10 +(num % 10);
        }

        static boolean chechPalindrome2(int num){

                System.out.println(reverseNumber(num));

                if(num == reverseNumber(num))
                        return true;
                
                return false;
        }


        public static void main(String[] args) {
                System.out.println(chechPalindrome1(1221));

                System.out.println(chechPalindrome2(121));
        }
}

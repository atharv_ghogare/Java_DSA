public class Program5 {
        static boolean checkPrime1(int num){
                
                for(int i = 2 ; i <= num/2 ; i++)
                        if(num % i == 0)
                                return false;

                return true;
        }

        static boolean checkPrime2(int num , int i){

                if( i > num / 2)
                        return true;
                
                if( num % i == 0)
                        return false;

                return checkPrime2(num, i + 1);

        }

        public static void main(String[] args) {
                
                System.out.println(checkPrime1(10));
                System.out.println(checkPrime1(11));

                System.out.println(checkPrime2(10 , 2));
                System.out.println(checkPrime2(11 , 2));

             
        }        
}

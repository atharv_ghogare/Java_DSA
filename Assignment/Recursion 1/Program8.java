/*
8. WAP to count the occurrence of a specific digit in a given number.
 */

public class Program8 {
        static int getCount1(int num , int key){
                int count = 0;
                
                while (num != 0) {
                
                        if( (num % 10) == key )
                                count++;

                        num = num / 10;
                }

                return count;
        }

        static int getCount2(int num , int key){

                if(num == 0)
                        return 0;
                
                if(num % 10 == key)
                        return 1 + getCount1(num / 10 , key);

                return getCount1(num / 10 , key);

        }

        public static void main(String[] args) {
                
                System.out.println(getCount1(444 , 5));

                System.out.println(getCount2(444 , 5));
        }         
}

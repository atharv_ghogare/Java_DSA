/*
1. WAP to print the numbers between 1 to 10.
 */

class Program1{

        static void printNum1(int num){
                for(int i = 1 ; i <= num ; i++)
                        System.out.println(i);
        }

        static void printNum2(int num){

                if(num == 0)
                        return;
                
                printNum2(num - 1);

                System.out.println(num);

        }

        public static void main(String[] args) {
                
                printNum1(10);

                printNum2(10);
        }
}
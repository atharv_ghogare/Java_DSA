/*
1. Reverse Integer (Leetcode:- 7)

Given a signed 32-bit integer x, return x with its digits reversed. If reversing
x causes the value to go outside the signed 32-bit integer range [-231, 231
- 1], then return 0.
Assume the environment does not allow you to store 64-bit integers (signed
or unsigned).

Example 1:
Input: x = 123
Output: 321

Example 2:
Input: x = -123 
Output: -321

Example 3:
Input: x = 120
Output: 21

Constraints:
-2^31 <= x <= 2^31 - 1
 */

class Reverse_Integer{

        static int reverse(int num){

                long temp = 0;

                while(num != 0){
                        temp = (temp * 10) + (num % 10);
                        num = num / 10;
                }


                if(temp > Integer.MAX_VALUE || temp < Integer.MIN_VALUE)
                        return 0;
                else
                        return (int)temp;
        }

        public static void main(String[] args) {
                
                System.out.println(reverse(123));
        }
}
/*
3. Search Insert Position (LeetCode-35)

Given a sorted array of distinct integers and a target value, return the index
if the target is found. If not, return the index where it would be if it were
inserted in order.
You must write an algorithm with O(log n) runtime complexity.
Example 1:
Input: nums = [1,3,5,6], target = 5
Output: 2
Example 2:
Input: nums = [1,3,5,6], target = 2
Output: 1
Example 3:
Input: nums = [1,3,5,6], target = 7
Output: 4

Constraints:
1 <= nums.length <= 10^4
-10^4 <= nums[i] <= 10^4
nums contains distinct values sorted in ascending order.
-10^4 <= target <= 10^4
 */

public class Search_Insert_Position {

        static int search_insert_position(int arr[] , int target){

                int start = 0;
                int end = arr.length - 1;
                int val = -1;

                while(start <= end){
                        int mid = (start + end)/2;

                        if(arr[mid] == target)
                                return mid;

                        else if(arr[mid] > target){
                                end = mid - 1;
                                val = mid;
                        }

                        else{
                                start = mid + 1;
                        }
                }

                return val;
        }

        public static void main(String[] args) {
                int arr[] = {1 , 3 , 5 , 9};

                System.out.println(search_insert_position(arr, 7));
        }
}


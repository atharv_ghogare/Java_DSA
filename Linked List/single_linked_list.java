
class Node{
	int data;

	Node next = null;

	Node(int data){
		this.data = data;
	}
}

class SinglyLinkedList{
	Node head = null;


	void print(){
		Node temp = head;

		while(temp.next != null){
			System.out.print(temp.data + " -> ");
			temp = temp.next;
		}
		
		System.out.println(temp.data);
	}

	int countNodes(){

		if(head == null)
			return 0;

		Node temp = head;
		int count = 0;

		while(temp.next != null){
			count++;
			temp = temp.next;
		}

		return count + 1;	
	}

	void addFirst(int data){
		Node temp = new Node(data);
	
		temp.next = head;
		head = temp;
	}

	void addLast(int data){
		Node temp = head;

		while(temp.next != null){
			temp = temp.next;
		}

		temp.next = new  Node(data);
	}

	void addAtPos(int pos , int data){
		Node temp = head;

		while(temp.next != null && pos - 2 > 0){
			temp = temp.next;
			pos--;	
		}

		Node newNode = new Node(data);
		newNode.next = temp.next;
		temp.next = newNode;
	}

	void deleteFirst(){
		if(head != null){
			head = head.next;
		}
	}


	void deleteLast(){
		if(head != null){

			if(head.next != null){

				Node temp = head;

				while(temp.next.next != null){
					temp = temp.next;
				}

				temp.next = null;

				return;
			}

			head = head.next;
		}
	}

	void deleteAtPos(int pos){
		
		if( head == null || pos > countNodes() || pos < 0) 
			return;

		if(pos == 0){
			deleteFirst();
			return;
		}

		Node temp = head;

		
		while(temp.next != null && pos - 3 > 0){
			temp = temp.next;
			pos--;
		}

		temp.next = temp.next.next;

	}
}

class Clint{
	public static void main(String[] args){
		SinglyLinkedList ll = new SinglyLinkedList();
	
		ll.addFirst(10);
		ll.addFirst(20);
		ll.addFirst(30);
		ll.addFirst(40);

		ll.addAtPos(2 , 100);

		ll.print();

		ll.deleteAtPos(0);

		ll.print();

		System.out.println(ll.countNodes());
	}
}
